package evaluator.Tests;

import evaluator.exception.DuplicateIntrebareException;
import evaluator.exception.InputValidationFailedException;
import evaluator.model.Intrebare;
import evaluator.repository.IntrebariRepository;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class IntrebariRepositoryTest {

    private Intrebare intrebare1;
    private IntrebariRepository intrebariRepository;

    @Before
    public void init(){
        intrebariRepository = new IntrebariRepository("F:\\VVSS\\5-ProiectEvaluatorExamen\\ProiectEvaluatorExamen\\src\\test\\java\\evaluator\\repository\\intrebariTest.txt");
    }

    @Test(expected = InputValidationFailedException.class)//BVA nonValid
    public void TestAddIntrebare0() throws InputValidationFailedException, IOException, DuplicateIntrebareException {
        intrebare1 = new Intrebare("","1)Eu","2)Tu","3)Ovidiu", "3", "Cinematografie");
        intrebariRepository.addIntrebare(intrebare1);
    }

    @Test(expected = InputValidationFailedException.class)//BVA nonValid
    public void TestAddIntrebare1() throws InputValidationFailedException, IOException, DuplicateIntrebareException {
        intrebare1 = new Intrebare("Cine era Florin Piersic?","1)euuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuu","2)TU","3)Ovidiu", "3", "Cinematografie");
        intrebariRepository.addIntrebare(intrebare1);
    }

    @Test //BVA valid
    public void TestAddIntrebare2() throws InputValidationFailedException, IOException, DuplicateIntrebareException {
        int currentSize = this.intrebariRepository.getIntrebari().size();
        intrebare1 = new Intrebare("Cine era Florin Piersic?","1)euuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuu","2)TU","3)Ovidiu", "3", "Cinematografie");
        intrebariRepository.addIntrebare(intrebare1);
        assertEquals(intrebariRepository.getIntrebari().size(), currentSize + 1);
    }

    @Test //BVA valid
    public void TestAddIntrebare3() throws InputValidationFailedException, IOException, DuplicateIntrebareException {
        int currentSize = this.intrebariRepository.getIntrebari().size();
        intrebare1 = new Intrebare("E?","1)Probabil","2)Se poate","3)N-ai cum", "2", "Existentialism");
        intrebariRepository.addIntrebare(intrebare1);
        assertEquals(intrebariRepository.getIntrebari().size(), currentSize + 1);
    }

    @Test(expected = InputValidationFailedException.class)//ECP nonValid
    public void TestAddIntrebare4() throws InputValidationFailedException, IOException, DuplicateIntrebareException {
        intrebare1 = new Intrebare("?","1)Da","2)Nu","3)Nu stiu, cumetre", "1", "Filosofie");
        intrebariRepository.addIntrebare(intrebare1);
    }

    @Test(expected = InputValidationFailedException.class)//ECP nonValid
    public void TestAddIntrebare5() throws InputValidationFailedException, IOException, DuplicateIntrebareException {
        intrebare1 = new Intrebare("Daca ploua, ne udam?","Da","2)Nu","3)Esti ud oricum", "2", "Meteorologie");
        intrebariRepository.addIntrebare(intrebare1);
    }

    @Test
    public void TestAddIntrebare6() throws InputValidationFailedException, IOException, DuplicateIntrebareException {//ECP valid
        int currentSize = this.intrebariRepository.getIntrebari().size();
        intrebare1 = new Intrebare("Sunt cine vreau sa fiu?","1)Depinde","2)Ai vrea tu","3)As vrea eu", "1", "Sociologie infantila");
        intrebariRepository.addIntrebare(intrebare1);
        assertEquals(intrebariRepository.getIntrebari().size(), currentSize + 1);
    }

    @Test
    public void TestAddIntrebare7() throws InputValidationFailedException, IOException, DuplicateIntrebareException {//ECP valid
        int currentSize = this.intrebariRepository.getIntrebari().size();
        intrebare1 = new Intrebare("Ne va fi mai bine?","1)Nu","2)Nu","3)Nu chiar", "3", "Despre Romania");
        intrebariRepository.addIntrebare(intrebare1);
        assertEquals(intrebariRepository.getIntrebari().size(), currentSize + 1);
    }

}