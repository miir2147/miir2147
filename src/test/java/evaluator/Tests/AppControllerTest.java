package evaluator.Tests;

import evaluator.controller.AppController;
import evaluator.exception.InputValidationFailedException;
import evaluator.exception.NotAbleToCreateTestException;
import evaluator.model.Intrebare;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.LinkedList;
import java.util.List;

public class AppControllerTest {

    private List<Intrebare> intrebari;
    private AppController appController = new AppController();
    private evaluator.model.Test test;

    @Before
    public void setUp() throws Exception {
        intrebari = new LinkedList<>();
        test = new evaluator.model.Test();
    }

    @Test(expected = NotAbleToCreateTestException.class)
    public void createNewTest1() throws NotAbleToCreateTestException {
        appController.createNewTest(intrebari);
    }

    @Test(expected = NotAbleToCreateTestException.class)
    public void createNewTest2() throws NotAbleToCreateTestException, InputValidationFailedException {
        Intrebare intrebare1 = new Intrebare("E?","1)Eu","2)Tu","3)Ovidiu", "3", "Cinematografie");
        Intrebare intrebare2 = new Intrebare("E?","1)Eu","2)Tu","3)Ovidiu", "3", "Cinematografie2");
        Intrebare intrebare3 = new Intrebare("E?","1)Eu","2)Tu","3)Ovidiu", "3", "Cinematografie3");
        Intrebare intrebare4 = new Intrebare("E?","1)Eu","2)Tu","3)Ovidiu", "3", "Cinematografie4");
        Intrebare intrebare5 = new Intrebare("E?","1)Eu","2)Tu","3)Ovidiu", "3", "Cinematografie4");

        intrebari.add(intrebare1);
        intrebari.add(intrebare2);
        intrebari.add(intrebare3);
        intrebari.add(intrebare4);
        intrebari.add(intrebare5);

        appController.createNewTest(intrebari);
    }

    @Test
    public void createNewTest3() throws NotAbleToCreateTestException, InputValidationFailedException {
        Intrebare intrebare1 = new Intrebare("E?","1)Eu","2)Tu","3)Ovidiu", "3", "Cinematografie");
        Intrebare intrebare2 = new Intrebare("E?","1)Eu","2)Tu","3)Ovidiu", "3", "Cinematografie2");
        Intrebare intrebare3 = new Intrebare("E?","1)Eu","2)Tu","3)Ovidiu", "3", "Cinematografie3");
        Intrebare intrebare4 = new Intrebare("E?","1)Eu","2)Tu","3)Ovidiu", "3", "Cinematografie4");
        Intrebare intrebare5 = new Intrebare("E?","1)Eu","2)Tu","3)Ovidiu", "3", "Cinematografie5");

        intrebari.add(intrebare1);
        intrebari.add(intrebare2);
        intrebari.add(intrebare3);
        intrebari.add(intrebare4);
        intrebari.add(intrebare5);

        test.setIntrebari(intrebari);
        appController.createNewTest(intrebari);
        Assert.assertEquals(intrebari, test.getIntrebari());
    }

    @Test
    public void createNewTest4() throws InputValidationFailedException, NotAbleToCreateTestException {
        Intrebare intrebare1 = new Intrebare("E?","1)Eu","2)Tu","3)Ovidiu", "3", "Cinematografie");
        Intrebare intrebare2 = new Intrebare("E?","1)Eu","2)Tu","3)Ovidiu", "3", "Cinematografie2");
        Intrebare intrebare3 = new Intrebare("E?","1)Eu","2)Tu","3)Ovidiu", "3", "Cinematografie3");
        Intrebare intrebare4 = new Intrebare("E?","1)Eu","2)Tu","3)Ovidiu", "3", "Cinematografie4");
        Intrebare intrebare5 = new Intrebare("E?","1)Eu","2)Tu","3)Ovidiu", "3", "Cinematografie5");
        Intrebare intrebare6 = new Intrebare("E?","1)Eu","2)Tu","3)Ovidiu", "3", "Cinematografie6");
        Intrebare intrebare7 = new Intrebare("E?","1)Eu","2)Tu","3)Ovidiu", "3", "Cinematografie7");
        Intrebare intrebare8 = new Intrebare("E?","1)Eu","2)Tu","3)Ovidiu", "3", "Cinematografie7");
        Intrebare intrebare9 = new Intrebare("E?","1)Eu","2)Tu","3)Ovidiu", "3", "Cinematografie8");
        Intrebare intrebare10 = new Intrebare("E?","1)Eu","2)Tu","3)Ovidiu", "3", "Cinematografie8");

        intrebari.add(intrebare1);
        intrebari.add(intrebare2);
        intrebari.add(intrebare3);
        intrebari.add(intrebare4);
        intrebari.add(intrebare5);
        intrebari.add(intrebare6);
        intrebari.add(intrebare7);
        intrebari.add(intrebare8);
        intrebari.add(intrebare9);
        intrebari.add(intrebare10);

        test = appController.createNewTest(intrebari);
//        Assert.assertEquals(test.getIntrebari().size(), 5);
    }
}