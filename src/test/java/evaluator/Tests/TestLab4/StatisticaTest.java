package evaluator.Tests.TestLab4;

import evaluator.controller.AppController;
import evaluator.exception.DuplicateIntrebareException;
import evaluator.exception.InputValidationFailedException;
import evaluator.exception.NotAbleToCreateStatisticsException;
import evaluator.model.Intrebare;
import evaluator.model.Statistica;
import evaluator.repository.IntrebariRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class StatisticaTest {

    private List<Intrebare> intrebari;
    private IntrebariRepository intrebariRepository = new IntrebariRepository("lab4.txt");
    private AppController appController = new AppController(intrebariRepository);

    @Before
    public void setUp() throws Exception {
        intrebari = new LinkedList<>();
    }

    @Test(expected = NotAbleToCreateStatisticsException.class)
    public void TestNonValidGetStatistica() throws NotAbleToCreateStatisticsException {
        appController.getStatistica();
    }

    @Test
    public void TestValidGetStatistica() throws NotAbleToCreateStatisticsException, InputValidationFailedException, IOException, DuplicateIntrebareException {
        Intrebare intrebare1 = new Intrebare("E?","1)Eu","2)Tu","3)Ovidiu", "3", "Cinematografie");
        intrebariRepository.addIntrebare(intrebare1);

        Statistica statistica = appController.getStatistica();
        Assert.assertEquals(statistica.getIntrebariDomenii().size() , 1);
    }


}