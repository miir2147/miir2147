package evaluator.Tests.TestLab4;

import evaluator.controller.AppController;
import evaluator.exception.DuplicateIntrebareException;
import evaluator.exception.InputValidationFailedException;
import evaluator.exception.NotAbleToCreateStatisticsException;
import evaluator.exception.NotAbleToCreateTestException;
import evaluator.model.Intrebare;
import evaluator.model.Statistica;
import evaluator.repository.IntrebariRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.*;
import java.util.LinkedList;
import java.util.List;

public class BigBangIntegrationTest {

    private List<Intrebare> intrebari;
    private IntrebariRepository intrebariRepository = new IntrebariRepository("lab4.txt");
    private AppController appController = new AppController(intrebariRepository);
    private evaluator.model.Test test;

    @Before
    public void setUp() throws Exception {
        intrebari = new LinkedList<>();
    }

    @Test
    public void TestValidGetStatistica() throws NotAbleToCreateStatisticsException, InputValidationFailedException, IOException, DuplicateIntrebareException {
        Intrebare intrebare1 = new Intrebare("E?","1)Eu","2)Tu","3)Ovidiu", "3", "Cinematografie");
        intrebariRepository.addIntrebare(intrebare1);

        Statistica statistica = appController.getStatistica();
        Assert.assertEquals(statistica.getIntrebariDomenii().size() , 1);
    }

    @Test
    public void createNewTest4() throws InputValidationFailedException, NotAbleToCreateTestException {

        Intrebare intrebare1 = new Intrebare("E?","1)Eu","2)Tu","3)Ovidiu", "3", "Cinematografie");
        Intrebare intrebare2 = new Intrebare("E?","1)Eu","2)Tu","3)Ovidiu", "3", "Cinematografie2");
        Intrebare intrebare3 = new Intrebare("E?","1)Eu","2)Tu","3)Ovidiu", "3", "Cinematografie3");
        Intrebare intrebare4 = new Intrebare("E?","1)Eu","2)Tu","3)Ovidiu", "3", "Cinematografie4");
        Intrebare intrebare5 = new Intrebare("E?","1)Eu","2)Tu","3)Ovidiu", "3", "Cinematografie5");
        Intrebare intrebare6 = new Intrebare("E?","1)Eu","2)Tu","3)Ovidiu", "3", "Cinematografie6");
        Intrebare intrebare7 = new Intrebare("E?","1)Eu","2)Tu","3)Ovidiu", "3", "Cinematografie7");
        Intrebare intrebare8 = new Intrebare("E?","1)Eu","2)Tu","3)Ovidiu", "3", "Cinematografie7");
        Intrebare intrebare9 = new Intrebare("E?","1)Eu","2)Tu","3)Ovidiu", "3", "Cinematografie8");
        Intrebare intrebare10 = new Intrebare("E?","1)Eu","2)Tu","3)Ovidiu", "3", "Cinematografie8");

        intrebari.add(intrebare1);
        intrebari.add(intrebare2);
        intrebari.add(intrebare3);
        intrebari.add(intrebare4);
        intrebari.add(intrebare5);
        intrebari.add(intrebare6);
        intrebari.add(intrebare7);
        intrebari.add(intrebare8);
        intrebari.add(intrebare9);
        intrebari.add(intrebare10);

        test = appController.createNewTest(intrebari);
        Assert.assertEquals(test.getIntrebari().size(), 5);
    }

    @Test
    public void TestAddIntrebare2() throws InputValidationFailedException, IOException, DuplicateIntrebareException {

        int currentSize = this.intrebariRepository.getIntrebari().size();
        Intrebare intrebare = new Intrebare("Cine era Florin Piersic?","1)euuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuu","2)TU","3)Ovidiu", "3", "Cinematografie");
        intrebariRepository.addIntrebare(intrebare);
        Assert.assertEquals(intrebariRepository.getIntrebari().size(), currentSize + 1);
    }

    @Test
    public void BigBangTesting() throws InputValidationFailedException, IOException, DuplicateIntrebareException, NotAbleToCreateTestException, NotAbleToCreateStatisticsException {

        int currentSize = this.intrebariRepository.getIntrebari().size();
        Intrebare intrebare = new Intrebare("Cine era Mihai Eminescu?","1)euuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuu","2)TU","3)Mihai", "3", "Literatura");
        appController.addNewIntrebare(intrebare);
        Assert.assertEquals(intrebariRepository.getIntrebari().size(), currentSize + 1);

        Intrebare intrebare1 = new Intrebare("E sau nu?","1)Eu","2)Tu","3)Ovidiu", "3", "Cinematografie");
        Intrebare intrebare2 = new Intrebare("E sau nu??","1)Eu","2)Tu","3)Ovidiu", "3", "Cinematografie2");
        Intrebare intrebare3 = new Intrebare("E sau nu??","1)Eu","2)Tu","3)Ovidiu", "3", "Cinematografie3");
        Intrebare intrebare4 = new Intrebare("E sau nu??","1)Eu","2)Tu","3)Ovidiu", "3", "Cinematografie4");
        Intrebare intrebare5 = new Intrebare("E sau nu??","1)Eu","2)Tu","3)Ovidiu", "3", "Cinematografie5");
        Intrebare intrebare6 = new Intrebare("E sau nu??","1)Eu","2)Tu","3)Ovidiu", "3", "Cinematografie6");
        Intrebare intrebare7 = new Intrebare("E sau nu??","1)Eu","2)Tu","3)Ovidiu", "3", "Cinematografie7");
        Intrebare intrebare8 = new Intrebare("E sau nu??","1)Eu","2)Tu","3)Ovidiu", "3", "Cinematografie8");
        Intrebare intrebare9 = new Intrebare("E sau nu??","1)Eu","2)Tu","3)Ovidiu", "3", "Cinematografie9");
        Intrebare intrebare10 = new Intrebare("E sau nu??","1)Eu","2)Tu","3)Ovidiu", "3", "Cinematografie10");

        appController.addNewIntrebare(intrebare1);
        appController.addNewIntrebare(intrebare2);
        appController.addNewIntrebare(intrebare3);
        appController.addNewIntrebare(intrebare4);
        appController.addNewIntrebare(intrebare5);
        appController.addNewIntrebare(intrebare6);
        appController.addNewIntrebare(intrebare7);
        appController.addNewIntrebare(intrebare8);
        appController.addNewIntrebare(intrebare9);
        appController.addNewIntrebare(intrebare10);

        test = appController.createNewTest(intrebariRepository.getIntrebari());
        Assert.assertEquals(test.getIntrebari().size(), 5);

        Statistica statistica = appController.getStatistica();
        Assert.assertEquals(statistica.getIntrebariDomenii().size() , 11);
    }
}
