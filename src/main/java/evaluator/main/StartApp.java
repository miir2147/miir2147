package evaluator.main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import evaluator.exception.DuplicateIntrebareException;
import evaluator.exception.InputValidationFailedException;
import evaluator.exception.NotAbleToCreateTestException;
import evaluator.model.Intrebare;
import evaluator.model.Statistica;

import evaluator.controller.AppController;
import evaluator.exception.NotAbleToCreateStatisticsException;
import evaluator.model.Test;
import evaluator.repository.IntrebariRepository;
//import org.testng.annotations.ITestAnnotation;

//functionalitati
//F01.	 adaugarea unei noi intrebari pentru un anumit domeniu (enunt intrebare, raspuns 1, raspuns 2, raspuns 3, raspunsul corect, domeniul) in setul de intrebari disponibile;
//F02.	 crearea unui nou test (testul va contine 5 intrebari alese aleator din cele disponibile, din domenii diferite);
//F03.	 afisarea unei statistici cu numarul de intrebari organizate pe domenii.

public class StartApp {

    private static final String file = "F:\\VVSS\\5-ProiectEvaluatorExamen\\ProiectEvaluatorExamen\\src\\main\\java\\intrebari.txt";

    public static void main(String[] args) throws IOException, InputValidationFailedException, DuplicateIntrebareException, NotAbleToCreateTestException {

        BufferedReader console = new BufferedReader(new InputStreamReader(System.in));

        IntrebariRepository intrebariRepository = new IntrebariRepository(file);
        AppController appController = new AppController(intrebariRepository);

        boolean activ = true;
        String optiune = null;

        appController.loadIntrebariFromFile();

        while (activ) {

            System.out.println("");
            System.out.println("1.Adauga intrebare");
            System.out.println("2.Creeaza test");
            System.out.println("3.Statistica");
            System.out.println("4.Exit");
            System.out.println("");

            optiune = console.readLine();

            switch (optiune) {
                case "1":
                    //modificat si aici
                    System.out.println("Introduceti enuntul noii intrebari: ");
                    String enunt = console.readLine();

                    System.out.println("Introduceti prima varianta a noii intrebari: ");
                    String varianta1 = console.readLine();

                    System.out.println("Introduceti a doua varianta a noii intrebari: ");
                    String varianta2 = console.readLine();

                    System.out.println("Introduceti a treia varianta a noii intrebari: ");
                    String varianta3 = console.readLine();

                    System.out.println("Introduceti varianta corecta a noii intrebari: ");
                    String variantaCorecta = console.readLine();

                    System.out.println("Introduceti domeniul noii intrebari: ");
                    String domeniu = console.readLine();

                    appController.addNewIntrebare(
                            new Intrebare(enunt, varianta1, varianta2, varianta3, variantaCorecta, domeniu));
                    break;

                case "2":
                    //modificari si pe aici
                    Test createdTest = appController.createNewTest(intrebariRepository.getIntrebari());
                    System.out.println(createdTest.toString());
                    break;
                case "3":
                    //mutat de aici
                    Statistica statistica;
                    try {
                        statistica = appController.getStatistica();
                        System.out.println(statistica);
                    } catch (NotAbleToCreateStatisticsException e) {
                        // TODO
                    }

                    break;
                case "4":
                    activ = false;
                    break;
                default:
                    break;
            }
        }

    }

}
