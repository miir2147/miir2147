package evaluator.controller;

import java.io.IOException;
import java.util.*;

import evaluator.model.Intrebare;
import evaluator.model.Statistica;
import evaluator.model.Test;
import evaluator.repository.IntrebariRepository;
import evaluator.exception.DuplicateIntrebareException;
import evaluator.exception.NotAbleToCreateStatisticsException;
import evaluator.exception.NotAbleToCreateTestException;

public class AppController {
	
	private IntrebariRepository intrebariRepository;
	
	public AppController() {
		intrebariRepository = new IntrebariRepository();
	}

	public AppController(IntrebariRepository intrebariRepository) {
		this.intrebariRepository = intrebariRepository;
	}

	public Intrebare addNewIntrebare(Intrebare intrebare) throws DuplicateIntrebareException, IOException {
		
		intrebariRepository.addIntrebare(intrebare);
		
		return intrebare;
	}
	
	public boolean exists(Intrebare intrebare){
		return intrebariRepository.exists(intrebare);
	}

	public int getNumberOfDistinctDomains(List<Intrebare> intrebari) {
		return getDistinctDomains(intrebari).size();

	}

	public Set<String> getDistinctDomains(List<Intrebare> intrebari) {
		Set<String> domains = new TreeSet<String>();
		for (Intrebare intrebre : intrebari)
			domains.add(intrebre.getDomeniu());
		return domains;
	}

	public Intrebare pickRandomIntrebare(List<Intrebare> intrebari) {
		Random random = new Random();
		return intrebari.get(random.nextInt(intrebari.size()));
	}

	public Test createNewTest(List<Intrebare> intrebari) throws NotAbleToCreateTestException{

		if(intrebari.size() < 5)
			throw new NotAbleToCreateTestException("Nu exista suficiente intrebari pentru crearea unui test!(5)");

		if(getNumberOfDistinctDomains(intrebari) < 5)
			throw new NotAbleToCreateTestException("Nu exista suficiente domenii pentru crearea unui test!(5)");

		List<Intrebare> testIntrebari = new LinkedList<Intrebare>();
		List<String> domenii = new LinkedList<String>();
		Intrebare intrebare;
		Test test = new Test();

		//modificat si aici
		while(testIntrebari.size() != 5){
			intrebare = pickRandomIntrebare(intrebari);

			if(!testIntrebari.contains(intrebare) && !domenii.contains(intrebare.getDomeniu())) {
				testIntrebari.add(intrebare);
				domenii.add(intrebare.getDomeniu());
			}
		}

		test.setIntrebari(testIntrebari);
		return test;
	}

	public void loadIntrebariFromFile(){
		intrebariRepository.setIntrebari(intrebariRepository.loadIntrebariFromFile());
	}
	
	public Statistica getStatistica() throws NotAbleToCreateStatisticsException{
		
		if(intrebariRepository.getIntrebari().isEmpty())
			throw new NotAbleToCreateStatisticsException("Repository-ul nu contine nicio intrebare!");
		
		Statistica statistica = new Statistica();
		for(String domeniu : intrebariRepository.getDistinctDomains()){
			//modificat si aici
			statistica.add(domeniu, intrebariRepository.getNumberOfIntrebariByDomain(domeniu));
		}
		
		return statistica;
	}

}
