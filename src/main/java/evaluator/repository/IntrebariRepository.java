package evaluator.repository;

import java.io.*;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;


import evaluator.model.Intrebare;
import evaluator.exception.DuplicateIntrebareException;

public class IntrebariRepository {

    private List<Intrebare> intrebari;
    private String file;

    public IntrebariRepository(String file) {
        this.intrebari = new LinkedList<>();
        this.file = file;
    }

    public IntrebariRepository() {
        setIntrebari(new LinkedList<Intrebare>());
    }

    public void addIntrebare(Intrebare i) throws DuplicateIntrebareException, IOException {
        if (exists(i))
            throw new DuplicateIntrebareException("Intrebarea deja exista!");
        intrebari.add(i);
        adaugaIntrebareInFisier(i);
    }

    private void adaugaIntrebareInFisier(Intrebare i) throws IOException {

        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file, true))) {
            bufferedWriter.write(i.getEnunt() + "\n");
            bufferedWriter.write(i.getVarianta1() + "\n");
            bufferedWriter.write(i.getVarianta2() + "\n");
            bufferedWriter.write(i.getVarianta3() + "\n");
            bufferedWriter.write(i.getVariantaCorecta() + "\n");
            bufferedWriter.write(i.getDomeniu() + "\n");
            bufferedWriter.write("## \n");
        }

    }

    public boolean exists(Intrebare i) {
        for (Intrebare intrebare : intrebari)
            if (intrebare.equals(i))
                return true;
        return false;
    }

    public Set<String> getDistinctDomains() {
        Set<String> domains = new TreeSet<String>();
        for (Intrebare intrebre : intrebari)
            domains.add(intrebre.getDomeniu());
        return domains;
    }

    public List<Intrebare> getIntrebariByDomain(String domain) {
        List<Intrebare> intrebariByDomain = new LinkedList<Intrebare>();
        for (Intrebare intrebare : intrebari) {
            if (intrebare.getDomeniu().equals(domain)) {
                intrebariByDomain.add(intrebare);
            }
        }

        return intrebariByDomain;
    }

    public int getNumberOfIntrebariByDomain(String domain) {
        return getIntrebariByDomain(domain).size();
    }

    public List<Intrebare> loadIntrebariFromFile() {

        List<Intrebare> intrebari = new LinkedList<Intrebare>();
        BufferedReader br = null;
        String line = null;
        List<String> intrebareAux;
        Intrebare intrebare;

        try {
            br = new BufferedReader(new FileReader(file));
            line = br.readLine();
            while (line != null) {
                intrebareAux = new LinkedList<String>();
                while (!line.equals("##")) {
                    intrebareAux.add(line);
                    line = br.readLine();
                }
                intrebare = new Intrebare();
                intrebare.setEnunt(intrebareAux.get(0));
                intrebare.setVarianta1(intrebareAux.get(1));
                intrebare.setVarianta2(intrebareAux.get(2));
                intrebare.setVarianta3(intrebareAux.get(3));

                //modificat si aici

                intrebare.setVariantaCorecta(intrebareAux.get(4));
                intrebare.setDomeniu(intrebareAux.get(5));
                intrebari.add(intrebare);
                line = br.readLine();
            }

        } catch (IOException e) {
            System.out.println("eroare: " + e);
        } finally {
            try {
                br.close();
            } catch (IOException e) {
                // TODO: handle exception
            }
        }

        return intrebari;
    }

    public List<Intrebare> getIntrebari() {
        return intrebari;
    }

    public void setIntrebari(List<Intrebare> intrebari) {
        this.intrebari = intrebari;
    }
}
